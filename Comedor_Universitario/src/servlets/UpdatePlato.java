package servlets;
import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import clases.*;

@SuppressWarnings("serial")
public class UpdatePlato extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		resp.setContentType("text/html");	
		String nombre = req.getParameter("nombre");		
		String descripcion = req.getParameter("descripcion");
		String foto = req.getParameter("foto");
		
		String idPlato = req.getParameter("idPlato");
		
		Key kPlato = (Key)KeyFactory.stringToKey((String)idPlato);
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();

		Transaction tx = pm.currentTransaction();
		tx.begin();
		
		try{
			
			Plato foundPlato = pm.getObjectById(Plato.class, kPlato);
			Plato p = new Plato(nombre,descripcion,foto,1);	
			
			try{
				foundPlato.setNombre(p.getNombre());
				foundPlato.setDescripcion(p.getDescripcion());
				foundPlato.setFoto(p.getFoto());
				
				
				tx.commit();
				resp.getWriter().println("Plato modificado correctamente.");
			}catch(Exception e){
				System.out.println(e);
				resp.sendRedirect("/platoServlet");
			}
			
		}catch(Exception e){
			System.out.println(e);
		}finally{
			try {
				if (tx.isActive())
					tx.rollback();
            } finally {
            	pm.close();
            }
		}
	}
}