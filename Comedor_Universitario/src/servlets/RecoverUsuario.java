package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import clases.*;

@SuppressWarnings("serial")
public class RecoverUsuario extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setContentType("text/html");

		String clave = (req.getParameter("clave"));
		Key key = KeyFactory.stringToKey(clave);
		

		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Usuario found = pm.getObjectById(Usuario.class, key);

		PrintWriter out = resp.getWriter();
			
		out.println("<form>");
		out.println("<input type='hidden' value='"+clave+ "' id='idUser' />");

		out.println("<div class='form-group'>");
		out.println("<label for='cui'><span class='glyphicon glyphicon-tag'></span> CUI</label>");
		out.println("<input type='number' class='form-control' id='cuiUpd' value='"+found.getCui()+"' placeholder='Ingrese codigo del alumno' required>");
		out.println("</div>");
		out.println("<div class='form-group'>");
		out.println("<label for='dni'><span class='glyphicon glyphicon-tag'></span> DNI</label>");
		out.println("<input type='number' class='form-control' id='dniUpd' value='"+found.getDni()+"' placeholder='Ingrese Documento de Identidad' required>");
		out.println("</div>");
		out.println("<div class='form-group'>");
		out.println("<label for='apellidop'><span class='glyphicon glyphicon-tag'></span> Apellido Paterno</label>");
		out.println("<input type='text' class='form-control' id='apaternoUpd' value='"+found.getPat()+"' placeholder='Ingrese Apellido Paterno' required>");
		out.println("</div>");
		out.println("<div class='form-group'>");
		out.println("<label for='apellidom'><span class='glyphicon glyphicon-tag'></span> Apellido Materno</label>");
		out.println("<input type='text' class='form-control' id='amaternoUpd' value='"+found.getMat()+"' placeholder='Ingrese Apellido Materno' required>");
		out.println("</div>");
		out.println("<div class='form-group'>");
		out.println("<label for='nombre'><span class='glyphicon glyphicon-tag'></span> Nombre</label>");
		out.println("<input type='text' class='form-control' id='nameUpd' value='"+found.getName()+"' placeholder='Ingrese Nombre' required>");
		out.println("</div>");
		out.println("<button type='submit' id='updUs' class='btn btn-success btn-block'><span class='glyphicon glyphicon-floppy-disk'></span> Modificar</button>");
		out.println("</form>");
				
	}
}
