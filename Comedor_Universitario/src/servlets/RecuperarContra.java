package servlets;

import clases.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Properties;
import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
public class RecuperarContra extends HttpServlet {

   @Override
   protected void doPost(HttpServletRequest req, HttpServletResponse resp)
           throws ServletException, IOException {
	   resp.setContentType("text/html");
	   PrintWriter out = resp.getWriter();
       String cui = req.getParameter("cui");
       String email = req.getParameter("email");
      
       Properties props = new Properties();
       Session session = Session.getDefaultInstance(props, null);
      
      
   		int  i=0;

		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Pensionista.class);
		List<Pensionista> user = (List<Pensionista>) q.execute(cui);
       

       try {
    	   
           
           for (Pensionista x : user) {
   			if (x.getCui().equals(cui)) {
   				
   				String pass = x.getPassword();
   				String name = x.getName();
   				String apellidos = x.getApellidos();
   				String msgBodyEmail = "Srs. "+ name + " " + apellidos + " su contraseña es: " + pass;
   			 Message msg = new MimeMessage(session);
             msg.setFrom(new InternetAddress("Info.SaludSoft@gmail.com","SaludSoft"));
             msg.addRecipient(Message.RecipientType.TO, new InternetAddress(email, cui));
             msg.setSubject("prueba");
             msg.setText(msgBodyEmail);
             Transport.send(msg);
   				
   				
   			out.println("<h2>Su contraseña a sido enviada a su correo satisfactoriamente");
   				
   			i++;
   				
   			}
   		}
   		if (i == 0) {
   			out.println("<h2>El Usuario no esta registrado."
   					+ " Porfavor ingrese uno correcto</h2>");
   			out.println("<meta http-equiv='Refresh' content='2;url=/es/admin/index.html'>");
   		}else{
   			out.println("<meta http-equiv='Refresh' content='2;url=/es/admin/index.html'>");
   		}

       } catch (Exception e) {
          
           resp.getWriter().println("HUBO FALLO.\n");
           resp.getWriter().println(e.getMessage());
       }
       resp.getWriter().println( "SE ENVIO EL EMAIL CORRECTAMENTE.");
   }
}