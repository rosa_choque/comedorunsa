package servlets;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import clases.*;

@SuppressWarnings("serial")
public class DeleteAllPensionistas extends HttpServlet{
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		resp.setContentType("text/plain");		
		PersistenceManager pm = PMF.get().getPersistenceManager();
		
		final Query q = pm.newQuery(Pensionista.class);
			try{
				q.deletePersistentAll();
				List<Pensionista> pensionistas = (List<Pensionista>) q.execute();
				req.setAttribute("pensionistas", pensionistas);
				resp.getWriter().println("Todos los Pensionistas fueron Borrados correctamente");
				
			}catch(Exception e){
					System.out.println(e);
					resp.getWriter().println("No se han podido borrar datos.");
					resp.sendRedirect("/indexAdmin.jsp");
			}finally{
				q.closeAll();
				pm.close();
			}				
	}
}

