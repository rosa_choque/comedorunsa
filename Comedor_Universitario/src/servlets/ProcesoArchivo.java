package servlets;


import java.io.IOException;
import java.util.Map;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.google.appengine.api.blobstore.BlobKey;
import com.google.appengine.api.blobstore.BlobstoreService;
import com.google.appengine.api.blobstore.BlobstoreServiceFactory;
import com.google.appengine.api.images.ImagesService;
import com.google.appengine.api.images.ImagesServiceFactory;
import com.google.appengine.api.images.ServingUrlOptions;





import javax.jdo.PersistenceManager;

@SuppressWarnings("serial")
public class ProcesoArchivo extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse resp)
	        throws IOException {

	    try {
	        Map<String, List<BlobKey>> files_sent = BlobstoreServiceFactory.getBlobstoreService().getUploads(req);
	        BlobKey file_uploaded_key = files_sent.get("myFile").get(0);
	        //resp.sendRedirect("/upload?blob_key=" + file_uploaded_key.getKeyString()); 
	        //resp.sendRedirect("/upload");
	        System.out.println("Document successfully POSTED, redirect to doGET");
	        
	        BlobKey blob_key = new BlobKey(file_uploaded_key.getKeyString());
	        BlobstoreServiceFactory.getBlobstoreService().serve(blob_key, resp);
	        String blob_key_string = file_uploaded_key.getKeyString();
	        
	        ImagesService imagesService = ImagesServiceFactory.getImagesService();
            ServingUrlOptions servingOptions = ServingUrlOptions.Builder.withBlobKey(blob_key);
	        String imageUrl = imagesService.getServingUrl(servingOptions);
	        
	        System.out.println("Blobkey given, sending file.");
	        System.out.println(imageUrl);
	        
	        HttpSession misesion = req.getSession();

	        misesion.setAttribute("imageUrl", imageUrl);
	        misesion.setAttribute("imageKey", blob_key_string);
	        resp.sendRedirect("/indexUsuario.jsp");
	        
	    }
	    catch (Exception e) {
	        resp.sendRedirect("/index.html");
	        System.out.println("Document failed to POST, redirecting back to upload.");
	    }
	}//end doPost
}
