package servlets;

//import java.awt.List;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.appengine.api.datastore.KeyFactory;

import clases.PMF;
import clases.Pensionista;
import clases.Usuario;



@SuppressWarnings("serial")
public class DeleteUsuario extends HttpServlet {
	@SuppressWarnings("unchecked")
	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
				
		String cui = req.getParameter("cuiDel");
				
		final PersistenceManager pm = PMF.get().getPersistenceManager();
		final Query q = pm.newQuery(Usuario.class);
		
		q.setFilter("cui == cuiParam");
		q.declareParameters("String cuiParam");
		
		try{			
			final Query q2 = pm.newQuery(Pensionista.class);
			List<Pensionista> p1 = (List<Pensionista>) q2.execute();
				
			for(Pensionista p: p1){
				if(p.getUser().getCui().equals(cui)){
					pm.deletePersistent(p);
					break;
				}						
			}
			
			List<Usuario> u = (List<Usuario>) q.execute(cui);	
			pm.deletePersistent(u.get(0));
			
			resp.getWriter().println("Usuario "+cui+ " Borrado con exito...");				
		
		}catch(Exception e){
			resp.getWriter().print("Ups .. ERROR");
		}finally{
			 q.closeAll();
			 pm.close();
		}
	}
}
