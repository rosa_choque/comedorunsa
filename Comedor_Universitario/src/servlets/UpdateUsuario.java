package servlets;
import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import clases.*;

@SuppressWarnings("serial")
public class UpdateUsuario extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		resp.setContentType("text/html");	
		String cui = req.getParameter("cui");		
		String name = req.getParameter("name");
		String apaterno = req.getParameter("apaterno");
		String amaterno = req.getParameter("amaterno");	
		String dni = req.getParameter("dni");
		String idUser = req.getParameter("idUser");
		
		Key kUsuario = (Key)KeyFactory.stringToKey((String)idUser);
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();

		Transaction tx = pm.currentTransaction();
		tx.begin();
		
		try{
			
			Usuario foundUsuario = pm.getObjectById(Usuario.class, kUsuario);
			Usuario u = new Usuario(cui,name,apaterno, amaterno,dni,1);	
			
			try{
				foundUsuario.setCui(u.getCui());
				foundUsuario.setName(u.getName());
				foundUsuario.setPat(u.getPat());
				foundUsuario.setMat(u.getMat());
				foundUsuario.setDni(u.getDni());
				
				tx.commit();
				resp.getWriter().println("Usuario modificado correctamente.");
			}catch(Exception e){
				System.out.println(e);
				resp.sendRedirect("/pensionistaServlet");
			}
			
		}catch(Exception e){
			System.out.println(e);
		}finally{
			try {
				if (tx.isActive())
					tx.rollback();
            } finally {
            	pm.close();
            }
		}
	}
}