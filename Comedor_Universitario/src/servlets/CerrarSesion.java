package servlets;

import java.io.IOException;

import javax.servlet.http.*;


@SuppressWarnings("serial")
public class CerrarSesion extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/html");
		HttpSession misesion= req.getSession();
		
		misesion.invalidate(); //Cierra la sesion
		resp.getWriter().println("Gracias!");
		resp.sendRedirect("index.html");
	}
}