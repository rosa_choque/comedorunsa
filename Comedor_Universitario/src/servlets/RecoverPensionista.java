package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import clases.*;

@SuppressWarnings("serial")
public class RecoverPensionista extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setContentType("text/html");

		String clave = (req.getParameter("clave"));
		Key key = KeyFactory.stringToKey(clave);
		

		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Pensionista found = pm.getObjectById(Pensionista.class, key);

		PrintWriter out = resp.getWriter();
			
		out.println("<form>");
		out.println("<input type='hidden' value='"+clave+ "' id='idPensionista' />");

		out.println("<div class='form-group'>");
		out.println("<label for='cui'><span class='glyphicon glyphicon-tag'></span> CUI</label>");
		out.println("<input type='text' class='form-control' value='"+found.getCui()+"' disabled='disabled'>");
		out.println("</div>");
		
		out.println("<div class='form-group'>");
		out.println("<label for='escuela'><span class='glyphicon glyphicon-tag'></span> Escuela</label>");
		out.println("<input type='text' class='form-control' id='escuelaUpd' value='"+found.getEscuela()+"' placeholder='Ingrese escuela de procedencia' required>");
		out.println("</div>");
		
		out.println("<div class='form-group'>");
		out.println("<label for='email'><span class='glyphicon glyphicon-tag'></span> e-mail</label>");
		out.println("<input type='text' class='form-control' id='emailUpd' value='"+found.getEmail()+"' placeholder='Ingrese correo electronico' required>");
		out.println("</div>");
		
		out.println("<div class='form-group'>");
		out.println("<label for='telefono'><span class='glyphicon glyphicon-tag'></span> Telefono</label>");
		out.println("<input type='number' class='form-control' id='numberUpd' value='"+found.getTelefono()+"' placeholder='Ingrese Telefono' required>");
		out.println("</div>");
		
		out.println("<div class='form-group'>");
		out.println("<label for='pass'><span class='glyphicon glyphicon-tag'></span> Password</label>");
		out.println("<input type='text' class='form-control' id='passUpd' value='"+found.getPassword()+"' placeholder='Ingrese Password' required>");
		out.println("</div>");
		
			
		out.println("<button type='submit' id='updPe' class='btn btn-success btn-block'><span class='glyphicon glyphicon-floppy-disk'></span> Modificar</button>");
		out.println("</form>");
				
	}
}
