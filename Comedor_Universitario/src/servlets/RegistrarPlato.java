package servlets;

import clases.*;

import java.io.IOException;
//import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@SuppressWarnings("serial")
public class RegistrarPlato extends HttpServlet {

	protected void doGet(HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
		
			resp.setContentType("text/html");
			//Verifica si esta registrado el usuario con el cui
			String nombre = req.getParameter("nombre");		
			PersistenceManager pm = PMF.get().getPersistenceManager();
			final Query q = pm.newQuery(Plato.class);
			List<Plato> platos = (List<Plato>) q.execute();
			String i="no encontrado";
			for(Plato pl : platos){
				String _nomb = pl.getNombre();
				if(nombre.equals(_nomb)){
					i="encontrado";
					break;
				}
			}
			if (i.equals("encontrado")) {
				resp.getWriter().println("Plato ya registrado, verifique porfavor");			
			}
			
			else{		
				String descripcion = req.getParameter("descripcion");
				String foto = req.getParameter("foto");
												
				Plato plato = new Plato(nombre,descripcion,foto,1);
							
				try{
					pm.makePersistent(plato);
					resp.getWriter().println("saving ...");		
				}catch(Exception e){
					System.out.println(e);
					resp.getWriter().println("Ocurrio un error, <a href='index.html'>vuelva a intentarlo</a>");
				}finally{
					pm.close();
				}
			}
	}
}
