<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="clases.*"%>
<%@ page import="java.util.List"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService" %>

<%
    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
%>

<%
	List<Nutricionista> nutricionistas = (List<Nutricionista>) request.getAttribute("nutricionistas");
int comida1 = 0, comida2 = 0, comida3 = 0, comida4 = 0;
Nutricionista nab=new Nutricionista("papa a la huancaina","caldo blanco","americano","rocoto relleno");
%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pagina de consulta del usuario">
    <meta name="author" content="">
	
    <title>Comedor Universitario</title>
	
    <!-- css -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="css/nivo-lightbox.css" rel="stylesheet" />
	<link href="css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
	<link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
    <link href="css/owl.theme.css" rel="stylesheet" media="screen" />
	<link href="css/flexslider.css" rel="stylesheet" />
	<link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
	<link href="color/default.css" rel="stylesheet">
	
	
	    <!-- Core JavaScript Files -->
    <script src="js/jquery.min.js"></script>	 
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.easing.min.js"></script>	
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/stellar.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>

    <script src="js/custom.js"></script>
    
    
	<style>
	table {
    border: 1px solid black;
    border-collapse: collapse;
}
	th, td {
    padding: 5px;
    text-align:center;
}
</style>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	
	
   	 <!-- Navigation -->
    <div id="navigation">
        <nav class="navbar navbar-custom" role="navigation">
                              <div class="container">
                                    <div class="row">
                                          <div class="col-md-2">
                                                   <div class="site-logo">
                                                            <a href="index.html" class="brand">Bienvenido "Nutricionista"</a>
                                                            
                                                  			
                                                    </div>
                                          </div>
                                          

                                          <div class="col-md-10">
                         
                                                      <!-- Brand and toggle get grouped for better mobile display -->
                                          <div class="navbar-header">
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                                                <i class="fa fa-bars"></i>
                                                </button>
                                          </div>
                             
                                          </div>
                                    </div>
                              </div>
                              <!-- /.container -->
                        </nav>
    </div> 
    <!-- /Navigation -->  

	<!-- Section: about -->
	
    <section id="about" class="home-section color-dark bg-white">
     
<!-- 		<form action="uploader.php" method="post" enctype="multipart/form-data"> -->
<!--     Select image to upload: -->
<!--     <input type="file" name="fileToUpload" id="fileToUpload"> -->
<!--     <input type="submit" value="Upload Image" name="submit"> -->
<!-- </form> -->
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Votos por Dia</h2>
					<div class="divider-header"></div>
					<p>Las comidas mas votadas hoy Lunes 18 de Julio del 2016.</p>
					</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container">

		
        <div class="row">
		
			<div class="col-md-6">
			    <img src="img/dummy1.jpg" alt="" class="img-responsive" />
      
           </div>
			<%
		     
		     	
				if (session.getAttribute("1") != null){
					comida1 = (Integer) session.getAttribute("1");
					}
				if (session.getAttribute("2") != null){
					comida2 = (Integer) session.getAttribute("2");
					}
				if (session.getAttribute("3") != null) {
					comida3 = (Integer) session.getAttribute("3");
					}
				if (session.getAttribute("4") != null){
					comida4 = (Integer) session.getAttribute("4");
					}
 			String c1=(Integer) session.getAttribute("pp1")+"%";
 			String c2=(Integer) session.getAttribute("pp2")+"%";
			String c3=(Integer) session.getAttribute("pp3")+"%";
 			String c4=(Integer) session.getAttribute("pp4")+"%";
			%>
            <div class="col-md-6">		
			<p>Porcentaje de las comidas de hoy.</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c1%>"><%=nab.getComida1()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c2%>"><%=nab.getComida2()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" 
			  aria-valuemin="0" aria-valuemax="100" style="width:<%=c3%>"><%=nab.getComida3()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c4%>"><%=nab.getComida4()%>
			  </div>
			</div>

            </div>
		

        </div>		
		</div>

	</section>
	<!-- /Section: about -->




<!-- Section: works -->								
    <section id="works" class="home-section color-dark text-center bg-white">

	
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">A�ade tu sugerencia para el menu del dia siguiente</h2>
					
					<div class="divider-header"></div>
					
					</div>
					</div>
    			 <form action="<%= blobstoreService.createUploadUrl("/procesoArchivo") %>" method="post" enctype="multipart/form-data">
  			        <br><br>
  			         <div class="col-md-6 xs-marginbot-20">
  			     <input type="file" name="myFile" ></div>
  			         <div class="col-md-6 xs-marginbot-20">
   			         <input type="submit" value="Submit"></div>
   			     </form>
   			     <br><br><br>
					<p>Votos realizados hasta el momento.</p>
												
				</div>
			</div>

		</div>
					


	</section>
	<!-- /Section: works -->

	


	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					
					<div class="text-center">
						<a href="#intro" class="totop"><i class="fa fa-angle-up fa-3x"></i></a>      
					</div>
				</div>
			</div>	
		</div>
	</footer>



</body>

</html>
